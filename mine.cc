/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"

// Network Topology
//
//                 10.1.100.0
// n3    n0      --------------      n1   n2
//  |	 |       point-to-point      |     |
//  ========                         ========
//LAN1 10.1.1.0                     LAN2 10.1.2.0

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MyFirstAssignment");

int
main(int argc, char* argv[])
{
    bool verbose = true;
    uint32_t sentPackets = 0;
    uint32_t receivedPackets = 0;
    uint32_t lostPackets = 0;

    CommandLine cmd(__FILE__);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.Parse(argc, argv);

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    NodeContainer csma1_nodes;
    csma1_nodes.Create (1);
    //0th is just csma
    //1st is csma and p2p

    NodeContainer csma2_nodes;
    csma2_nodes.Create (1);

    NodeContainer p2p_nodes;
    p2p_nodes.Create (2);

    CsmaHelper csma1;
    csma1.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
    csma1.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
    csma1_nodes.Add (p2p_nodes.Get (0));
    NetDeviceContainer csma1_devices;
    csma1_devices = csma1.Install (csma1_nodes);

    CsmaHelper csma2;
    csma2.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
    csma2.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
    csma2_nodes.Add (p2p_nodes.Get (1));
    NetDeviceContainer csma2_devices;
    csma2_devices = csma2.Install (csma2_nodes);

    PointToPointHelper p2p;
    p2p.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));
    p2p.SetChannelAttribute ("Delay", StringValue ("2ms"));
    NetDeviceContainer p2p_devices;
    p2p_devices = p2p.Install (p2p_nodes);

    InternetStackHelper stack;
    stack.Install (csma1_nodes);
    stack.Install (csma2_nodes);

    //on IPv4
    Ipv4AddressHelper address;
    address.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer csma1_interfaces;
    csma1_interfaces = address.Assign (csma1_devices);
    Address serverOnCsma1_address = csma1_interfaces.GetAddress(1);

    address.SetBase ("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer csma2_interfaces;
    csma2_interfaces = address.Assign (csma2_devices);
    Address serverOnCsma2_address = csma2_interfaces.GetAddress(1);

    address.SetBase ("10.1.100.0", "255.255.255.0");
    Ipv4InterfaceContainer p2p_interfaces;
    p2p_interfaces = address.Assign (p2p_devices);

    // udp server on csma1
    UdpEchoServerHelper serverOnCsma1 (9);
    ApplicationContainer appsOnCsma1 = serverOnCsma1.Install (csma1_nodes.Get(1));
    appsOnCsma1.Start (Seconds (1.0));
    appsOnCsma1.Stop (Seconds (10.0));

    // udp server on csma2
    UdpEchoServerHelper serverOnCsma2 (9);
    ApplicationContainer appsOnCsma2 = serverOnCsma2.Install (csma2_nodes.Get(1));
    appsOnCsma2.Start (Seconds (1.0));
    appsOnCsma2.Stop (Seconds (10.0));

    // upd client on csma1
    UdpEchoClientHelper clientOnCsma1 (serverOnCsma1_address, 9);
    clientOnCsma1.SetAttribute ("MaxPackets", UintegerValue (1));
    clientOnCsma1.SetAttribute ("Interval", TimeValue (MilliSeconds (1.0)));
    clientOnCsma1.SetAttribute ("PacketSize", UintegerValue (1024));
    ApplicationContainer clientOnCsma1_Apps = clientOnCsma1.Install(csma1_nodes.Get(0));
    clientOnCsma1_Apps.Start(Seconds(2.0));
    clientOnCsma1_Apps.Stop(Seconds(100.0));

    // upd client on csma1
    UdpEchoClientHelper clientOnCsma2 (serverOnCsma2_address, 9);
    clientOnCsma2.SetAttribute ("MaxPackets", UintegerValue (1));
    clientOnCsma2.SetAttribute ("Interval", TimeValue (MilliSeconds (1.0)));
    clientOnCsma2.SetAttribute ("PacketSize", UintegerValue (1024));
    ApplicationContainer clientOnCsma2_Apps = clientOnCsma2.Install(csma2_nodes.Get(0));
    clientOnCsma2_Apps.Start(Seconds(2.0));
    clientOnCsma2_Apps.Stop(Seconds(100.0));

    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
    csma1.EnablePcap("csma1", csma1_devices);
    csma2.EnablePcap("csma2", csma2_devices);
    p2p.EnablePcapAll("p2p");

    FlowMonitorHelper flowmon;
    Ptr<FlowMonitor> monitor = flowmon.InstallAll();

    Simulator::Stop (Seconds (20));
    Simulator::Run ();

    int j=0;
    float AvgThroughput = 0;
    Time Jitter;
    Time Delay;

    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

        NS_LOG_UNCOND("----Flow ID:" <<iter->first);
        NS_LOG_UNCOND("Src Addr" <<t.sourceAddress << "Dst Addr "<< t.destinationAddress);
        NS_LOG_UNCOND("Sent Packets=" <<iter->second.txPackets);
        NS_LOG_UNCOND("Received Packets =" <<iter->second.rxPackets);
        NS_LOG_UNCOND("Lost Packets =" <<iter->second.txPackets-iter->second.rxPackets);
        NS_LOG_UNCOND("Packet delivery ratio =" <<iter->second.rxPackets*100/iter->second.txPackets << "%");
        NS_LOG_UNCOND("Packet loss ratio =" << (iter->second.txPackets-iter->second.rxPackets)*100/iter->second.txPackets << "%");
        NS_LOG_UNCOND("Delay =" <<iter->second.delaySum);
        NS_LOG_UNCOND("Jitter =" <<iter->second.jitterSum);
        NS_LOG_UNCOND("Throughput =" <<iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024<<"Kbps");

        sentPackets = sentPackets +(iter->second.txPackets);
        receivedPackets = receivedPackets + (iter->second.rxPackets);
        lostPackets = lostPackets + (iter->second.txPackets-iter->second.rxPackets);
        AvgThroughput = AvgThroughput + (iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024);
        Delay = Delay + (iter->second.delaySum);
        Jitter = Jitter + (iter->second.jitterSum);

        j = j + 1;

    }

    AvgThroughput = AvgThroughput/j;
    NS_LOG_UNCOND("--------Total Results of the simulation----------"<<std::endl);
    NS_LOG_UNCOND("Total sent packets  =" << sentPackets);
    NS_LOG_UNCOND("Total Received Packets =" << receivedPackets);
    NS_LOG_UNCOND("Total Lost Packets =" << lostPackets);
    NS_LOG_UNCOND("Packet Loss ratio =" << ((lostPackets*100)/sentPackets)<< "%");
    NS_LOG_UNCOND("Packet delivery ratio =" << ((receivedPackets*100)/sentPackets)<< "%");
    NS_LOG_UNCOND("Average Throughput =" << AvgThroughput<< "Kbps");
    NS_LOG_UNCOND("End to End Delay =" << Delay);
    NS_LOG_UNCOND("End to End Jitter delay =" << Jitter);
    NS_LOG_UNCOND("Total Flod id " << j);
    monitor->SerializeToXmlFile("mine.xml", true, true);

    Simulator::Destroy ();
    return 0;
}

